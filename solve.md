# Problem to Solve

Create a dataset of recipe instructions that may or may not be swappable.
This dataset can then later be put into a web app for distribution.

## Meta-Steps

- Use the Recipe-NLG dataset to fetch recipes.
- Preprocess accordingly.

## Steps

- [x] Get the dataset from the huggingface datasets package.
- [x] Turn each datapoint into an instruction (including "get {ingredient}").
- [x] Run a classification task on each instruction to see which entity it really
  corresponds to.
- [x] Generate the pairs of instructions to test.
- [ ] Implement everything in a script.

## Infrastructure

- $9/mo VPS (b/c background jobs)
