from sentence_transformers import SentenceTransformer, util
from scipy.stats import t as tdist
import numpy as np
import torch
from os import getpid

from typing import Any
from .recipe import Recipe, RecipeQuestion


RELEVANT_THRESHOLD = 0.2
model = SentenceTransformer('all-MiniLM-L6-v2')


# Get p values from a T distribution of a numpy array (lower = better)
def pvalues(x):
    if x.size > 1:
        statistics = (x - np.mean(x))/np.std(x)
        # the p-value should be inverted (because higher statistics=better)
        return 1 - tdist.cdf(statistics, df=x.size - 1)
    else:
        return np.array([0])


RECIPE_COUNT = 0


def generate_from_recipe(recipe: Recipe) -> 'dict':
    global RECIPE_COUNT
    RECIPE_COUNT += 1
    print("Processing Recipe", RECIPE_COUNT, "from thread", getpid())

    ingredient_embeddings: Any = model.encode(recipe.ingredients,
                                              convert_to_numpy=True)
    instruction_embeddings = model.encode(recipe.instructions,
                                          convert_to_numpy=True)
    # a list of list of steps which are relevant for each recipe entity.
    steps_per_ingredient = [
        [Recipe.ingredient_to_instruction(ingredient)]
        for ingredient in recipe.ingredients
    ]

    for instruction_index, instruction_embedding in enumerate(instruction_embeddings):
        # get cosine similarities with each of the ingredients.
        similarities = np.array(util.cos_sim(instruction_embedding,
                                             ingredient_embeddings))[0]
        # get pvalues of similarities in a t-distribution.
        similarities = pvalues(similarities)
        # turn the array into a binary of whether or not the p-value is less
        # than the threshold for relevancy.
        similarities = np.vectorize(
            lambda x: 1 if x < RELEVANT_THRESHOLD else 0
        )(similarities)

        # add to the steps of the ingredient if relevant.
        for ingredient_index, relevant in enumerate(similarities):
            if relevant:
                steps_per_ingredient[ingredient_index].append(
                    recipe.instructions[instruction_index])

    # generate all of the recipe questions.
    questions = [
        RecipeQuestion(title=recipe.title,
                       ingredient=recipe.ingredients[ingredient_index],
                       instructions=(steps[i], steps[j]))
        for ingredient_index, steps in enumerate(steps_per_ingredient)
        for i in range(len(steps))
        for j in range(i)
    ]

    return dict(
        title=[current_recipe.title for current_recipe in questions],
        instruction=[
            current_recipe.instructions for current_recipe in questions],
        ingredient=[
            current_recipe.ingredient for current_recipe in questions],
    )

    # returns a dictionary of instructions based on a CSV batch.


def map_csv_row_batch(csv_batch: 'dict') -> 'dict':
    res = dict(
        title=[],
        instruction=[],
        ingredient=[]
    )
    for title, ingredients, directions in zip(csv_batch['title'], csv_batch['ingredients'], csv_batch['directions']):
        recipe = Recipe.from_csv_row(
            dict(ingredients=ingredients, title=title, directions=directions))
        # result from the above function.
        d = generate_from_recipe(recipe)
        res['title'] += d['title']
        res['instruction'] += d['instruction']
        res['ingredient'] += d['ingredient']
    return res
