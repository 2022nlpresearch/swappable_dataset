#!/usr/bin/env python3

import os
from datasets.load import load_dataset


def recipe_dataset():
    dataset_path = os.path.join(os.path.dirname(__file__), "..", "recipenlg")
    print("Loading the recipe_nlg dataset ...")
    return load_dataset("recipe_nlg", data_dir=dataset_path,
                        split='train[:40%]')
