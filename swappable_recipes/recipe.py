from dataclasses import dataclass


@dataclass(frozen=True)
class Recipe:
    title: str
    instructions: 'list[str]'
    ingredients: 'list[str]'

    # returns an instruction corresponding to an ingredient (aka "get it").
    @classmethod
    def ingredient_to_instruction(cls, ingredient:str) -> str:
        return 'Get ' + ingredient + '.'
    
    # take in a CSV row (as a dictionary) and return a Recipe object.
    @classmethod
    def from_csv_row(cls, csv_row: 'dict') -> 'Recipe':
        # return a recipe object containing these new parameters.
        return Recipe(
            title=csv_row['title'],
            instructions=csv_row['directions'],
            ingredients=csv_row['ingredients']
        )



@dataclass
class RecipeQuestion:
    title: str
    instructions: 'tuple[str,str]'
    ingredient: str

    def to_dict(self):
        return dict(title=self.title, instruction1=self.instructions[0],
                    instruction2=self.instructions[1])
