#!/usr/bin/env python3

import os
from swappable_recipes.download import recipe_dataset
from swappable_recipes.generate import map_csv_row_batch
from typing import Any

def main():
    path = os.path.join(os.path.dirname(__file__), "recipe_dataset")
    os.makedirs(path, exist_ok=True)

    original_dataset: Any = recipe_dataset()
    dataset = original_dataset.map(map_csv_row_batch,
                              remove_columns=original_dataset.column_names,
                              batched=True,
                              )
    dataset.to_csv(os.path.join(path, "recipe_dataset.csv"))
    dataset.save_to_disk(os.path.join(path, "recipe_dataset"))

if __name__ == "__main__":
    main()
