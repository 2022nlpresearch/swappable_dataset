# Swappable Recipe Dataset Generator

## Installing the dataset

1. Download the dataset into this directory from [this
   link.](https://recipenlg.cs.put.poznan.pl/dataset)
2. Unzip the archive.
3. Move the resulting folder to `recipenlg` so that the file
   `$(repository)/recipenlg/full_dataset.csv` exists.
